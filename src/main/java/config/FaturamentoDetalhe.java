package config;

import java.util.Date;

import edu.cest.model.Estadia;
import edu.cest.model.Funcionario;
import edu.cest.model.Vaga;
import edu.cest.model.Veiculo;

public class FaturamentoDetalhe {

	private int idfaturamento;

	private Funcionario f;

	private Estadia e;

	private Double valor;

	private String observacao;

	private Date dataHoraPagamento;
	
	private Veiculo veiculo;
	
	private Vaga vaga;

	public int getIdfaturamento() {
		return idfaturamento;
	}

	public void setIdfaturamento(int idfaturamento) {
		this.idfaturamento = idfaturamento;
	}

	public Funcionario getF() {
		return f;
	}

	public void setF(Funcionario f) {
		this.f = f;
	}

	public Estadia getE() {
		return e;
	}

	public void setE(Estadia e) {
		this.e = e;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Date getDataHoraPagamento() {
		return dataHoraPagamento;
	}

	public void setDataHoraPagamento(Date dataHoraPagamento) {
		this.dataHoraPagamento = dataHoraPagamento;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Vaga getVaga() {
		return vaga;
	}

	public void setVaga(Vaga vaga) {
		this.vaga = vaga;
	}
	
	

}
