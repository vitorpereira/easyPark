package edu.cest.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import edu.cest.model.Funcionario;

public class FuncionarioRowMapper implements RowMapper<Funcionario>{

	@Override
	public Funcionario mapRow(ResultSet rs, int rowNum) throws SQLException {
		Funcionario f = new Funcionario();
		
		f.setId(rs.getInt("idfuncionario"));
		f.setNome(rs.getString("nome")); 
		f.setTelefone(rs.getString("telefone")); 
		f.setAdministrador(rs.getBoolean("isAdministrador"));	
		
		return f;
	}

}

