package edu.cest.dao;

import java.util.List;

import edu.cest.model.Funcionario;

public interface FuncionarioDAO {
	
	/**
	 * 
	 * @return Retorna uma lista de funcionarios
	 */
	List<Funcionario> findAll();

	/**
	 * 
	 * @param user - nome do usuário
	 * @param pass - senha do usuário
	 * @return - Objeto Funcionário
	 */
	Funcionario findByLogin(String user, String pass);
	
	/**
	 * 
	 * @param f - Funcionario a ser persistido
	 * @return
	 */
	int save(Funcionario f);
	
	/**
	 * 
	 * @param id - id do funcionario que será excluido
	 * @return
	 */
	int delete(Integer id);
	
	
	/**
	 * Busca funcionário por ID
	 * @param id - Id do Funcionário
	 * @return Funcionário
	 */
	Funcionario findById(Integer id);
	
	
}
